


import 'package:datatransport/datatransport.dart';
import 'package:rxdart/subjects.dart';

abstract class IDTCInput<I>
{
 PublishSubject<I> get input;
}
abstract class IDTCOutput<O>
{
 PublishSubject<O> get output;
}

abstract class IDTCBidirectional<I,O> implements IDTCInput<I>, IDTCOutput<O>
{
  
  ///the rule by which data is processed and transmitted to the outgoing stream.
  void setRule(O? Function(I input)? rule);
}


///Data Transport Channel for universal data
class DTChannelUniversal<I,O> implements IDTCBidirectional<I,O>
{
  final PublishSubject<I> _input = PublishSubject<I>();
  PublishSubject<I> get input =>_input;
  final PublishSubject<O> _output = PublishSubject<O>();
  PublishSubject<O> get output => _output;
  O? Function(I input)? _rule;
  bool _isEqual = false;
  DTChannelUniversal()
  {
     _input.listen((value) {
      _processing(value);
    });
    if(I.runtimeType == O.runtimeType)
      _isEqual= true;
  }
  void setRule(O? Function(I input)? rule)
  {
    _rule = rule;
  }

  void _processing(I input) {
    // if(!_isBlock)
    {
    // if (_rule == null) {
    //    if(_isEqual)
    //      _output.add(input as O);
     
    // }
    // else
    if (_rule != null)
    {
      var tmp = _rule!.call(input);
      if(tmp!=null)
      {
      _output.add(tmp);
      }
    }
  }
}
}

/// 
class DTChannelBasic implements IDTCBidirectional<DTOBasic,DTOBasic>
{
  final PublishSubject<DTOBasic> _input = PublishSubject<DTOBasic>();
  PublishSubject<DTOBasic> get input =>_input;
  final PublishSubject<DTOBasic> _output = PublishSubject<DTOBasic>();
  PublishSubject<DTOBasic> get output => _output;
  DTOBasic? Function(DTOBasic input)? _rule;

  DTChannelBasic()
  {
    _input.listen((value) {
      _processing(value);
    });
  }
  void setRule(DTOBasic? Function(DTOBasic input)? rule)
  {
    _rule = rule;
  }

  void _processing(DTOBasic input) {
    // if(!_isBlock)
    {
    if (_rule == null) {
     
        _output.add(input);
     
    }
    else
    {
      var tmp = _rule!.call(input);
      if(tmp!=null)
      {
      _output.add(tmp);
      }
    }
  }
}
}

