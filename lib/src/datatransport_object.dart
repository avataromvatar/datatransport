
abstract class IDTO
{
  String? get source;
  String get type;
  Map<String,dynamic>? get data;
}

class DTO_Basic_TYPE
{
  static const String CREATE = 'create';
  static const String READ = 'read';
  static const String UPDATE = 'update';
  static const String UPDATE_OR_CREATE = 'update_create';
  static const String DELETE = 'delete';
  
}
class DTOBasic implements IDTO
{

  final String? source;
  // final String target;
  final String type;
  final Map<String,dynamic>? data;
  

  DTOBasic(this.type,{this.source,this.data});
  /// data must have field 'name' = inital_data? 
  /// return in [output] stream if all good, input DTO
  // DTOBasic.create({this.source,required this.data}):type=DTO_Basic_TYPE.CREATE;
  /// if [data] null read all data else only data['need']=List < String >.  
  /// return in output stream dto.read whe dto.data have all data or need data.
  /// if you set need key in input stream and in output stream you not see key -> model not have this data
  // DTOBasic.read({this.source,this.data}):type=DTO_Basic_TYPE.READ;
  /// dto.data must have data to change in model, if model not have data to change new field NOT CREATE
  // DTOBasic.update({this.source,required this.data}):type=DTO_Basic_TYPE.UPDATE;
  /// dto.data must have data in model, if model not have data to change new field CREATE
  // DTOBasic.update_create({this.source,required this.data}):type=DTO_Basic_TYPE.UPDATE_OR_CREATE;
  /// dto.data must have data['to delete'] = List < String > in model 
  // DTOBasic.delete({this.source,required this.data}):type=DTO_Basic_TYPE.DELETE;

}

class DTOBasicByTarget extends DTOBasic
{
  final String target;
  
  DTOBasicByTarget(this.target,String type,{String? source,Map<String,dynamic>? data}):super(type,source: source,data: data);

}